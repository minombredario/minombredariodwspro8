 <?php 
   
    class Controlador{
        
        function inicio(){
           
            require __DIR__ . '/templates/VistaEleccion.php';
        }
        
        
        function error() {
            require __DIR__ . '/templates/error.php';
        }
        
        
        function menu(){
            if (getModelo() != null) {
                    $params['mensaje'] = array('texto'=> "");
                    require __DIR__ . '/templates/VistaMenu.php';
                } else {
                    require __DIR__ . '/templates/VistaEleccion.php';
                }
        }
        
        
        function instalar() {
            
            if (getModelo() != null) {
                $modelo = getModelo();
                
                if ($modelo->existe()) {
                    $modelo->instalarBD();
                    $params = array('instalar' => $modelo->instalarBD(),
                                    'tablas' => $modelo->CrearTablas(), 
                                    'claves' => $modelo->CrearClaves());
                    
                    $params['mensaje'] = array(
                                                'instalar' => 'Base de datos ' . $_SESSION['modelo'] . ' instalada .', 
                                                'claves'=> 'Creada CAj: Asignaturas.id_profesor-> profesores.id',
                                                'tablas' => 'Creada tabla: asignaturas<br>Creada tabla: profesores<br>');
                    require __DIR__ . '/templates/VistaInstalacion.php';
                }
            }else{
                require __DIR__ . '/templates/error.php';
            }
        }
        
        function ejemplo(){
            if (getModelo() != null) {
                $modelo = getModelo();
                 //var_dump($modelo->comprobar());
                if ($modelo->existe() && $modelo->comprobar()==false) {
                   
                    $modelo->rellenarTablas();
                    
                    require __DIR__ . '/templates/VistaMenu.php';
                    
                }else{
                    $params['mensaje'] = array('texto' => 'BBDD creada con Ejemplos');
                    require __DIR__ . '/templates/VistaMenu.php';
                    
                }
                
            }else{
                require __DIR__ . '/templates/error.php';
            }
                
                
        }
        
        function login() {
            $modelo = recoge("modelo");
            $_SESSION["modelo"]= $modelo;
            //var_dump($modelo);
            $vista = recoge("modelo");
            require __DIR__.'/templates/VistaLogin.php';
           
        }
        
        function registro(){
            if (getModelo() != null) {
                $modelo = getModelo();
                require __DIR__.'/templates/VistaRegistro.php';
            }else{
                $params['mensaje'] = array('texto'=> "ERROR: No ha seleccionado ninguna base de datos");
                require __DIR__ . '/templates/VistaEleccion.php';
            }
            
            
        }
        
        
        
        function logout() {
            unset($_SESSION["usuario"]);
            session_destroy();

            require __DIR__ . '/templates/VistaEleccion.php';
        }
        
                     
       
    }   
    ?>                 
        

