<?php

    function recoge($valor) {
        $resultado = "";

        if (isset($_REQUEST[$valor])) {
            $resultado = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])), ENT_QUOTES, "UTF-8");
        }

        return $resultado;
    }

    function getModelo(){
        if(!isset($_SESSION)) { session_start();}
            $modelo = "";
        if(isset($_SESSION["modelo"])){
            if($_SESSION["modelo"] == "Sql"){
                $modelo = new ModeloSQL();
            }else if($_SESSION["modelo"] == "Ficheros"){
                $modelo = new ModeloFichero();
        }
        
        return $modelo;
        }
    }
    
    function CerrarSesion(){
        if(!isset($_SESSION)) { session_start();  }
        session_destroy();
        header("Location:../vista/VistaEleccion.php");
    }
    
    function conectar(){
        
        if(!isset($_SESSION)) { 
            session_start();  
        
            if(isset($_SESSION["usuario"])){
                $_SESSION["usuario"];
            }else{
                include ("Config.php");
                require __DIR__ . '/templates/VistaEleccion.php';
            }
        
        }else {
            if(isset($_SESSION["usuario"])){
                $_SESSION["usuario"];
            }else{
                include ("Config.php");
                require __DIR__ . '/templates/VistaEleccion.php';
            }
        }
    }
  
        
        
    
    
 
