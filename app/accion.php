<?php

    class accion{

                //      USUARIOS        //
        function loguearse(){
            if (getModelo() != null) {
                $modelo = getModelo();
                                
                $nombre = strtolower(recoge('usuario'));
                $pass= recoge('password');
                $usuario= new Usuario("",$nombre,$pass);

            //echo $usuario->getId()."<br>".$usuario->getUsuario()."<br>".$usuario->getPass();
                $comprobar = $modelo->ComprobarUsuario($usuario);

                if($comprobar == null){
                    $params['mensaje'] = array('texto'=>"Usuario no registrado");
                    require __DIR__ . '/templates/VistaLogin.php';

                }else{
                    $_SESSION["usuario"]=$nombre;
                    
                    require __DIR__ . '/templates/VistaMenu.php';

                }
            }else{
                    require __DIR__ . '/templates/VistaEleccion.php';
                   
                }

        }
        
        function registrar(){
            if (getModelo() != null) {
                $modelo = getModelo();
                
                $nombre = strtolower(recoge('usuario'));
                $pass= recoge('password');
                
                $usuario= new Usuario("",$nombre,$pass);
                
                //echo $usuario->getId()."<br>".$usuario->getUsuario()."<br>".$usuario->getPass();
                $comprobar = $modelo->ComprobarUsuario($usuario);
                if($comprobar == null){
                    $modelo->createUsuario($usuario);
                    $params['mensaje'] = array('texto'=> "Usuario $nombre ha sido registrado con exito.");
                    require __DIR__.'/templates/VistaRegistro.php';
                    
                }
                    $params['mensaje'] = array('texto'=> "El usuario y la contraseña ya está registrado.");
                    require __DIR__.'/templates/VistaRegistro.php';
                    
                
            }else{
                $params['mensaje'] = array('texto'=> "ERROR: No ha seleccionado ninguna base de datos");
                require __DIR__ . '/templates/VistaEleccion.php';
            }
            
            
        }
        
       
        
                //      ACCIONES PROFESOR       //
        
        function readprof(){
            if (getModelo() != null) {
                $modelo = getModelo();
                if ($modelo->existe()) {
                    $params = array('profesores' => $modelo->readProfesor(), 'id' =>$modelo->getId("profesores"));
                    require __DIR__ . '/templates/VistaProfesores.php';
                } else {
                    require __DIR__ . '/templates/error.php';
                }
            }else {
                require __DIR__ . '/templates/error.php';
            }
        }
        
        function delprof(){
            $id = recoge('id');
                if (getmodelo() != null) {
                    $modelo = getModelo();
                if ($modelo->existe()) {
                    $profesor = new Profesor($id, "");
                    $modelo->deleteProfesor($profesor);
                    $params = array('profesores' => $modelo->readProfesor(), 'id' =>$modelo->getId("profesores"));
                    $params['mensaje'] = array('tipo' => 'success', 'texto' => 'Profesor ' . $profesor->getNombre() . ' eliminado con éxito.');
                    require __DIR__ . '/templates/VistaProfesores.php';
                } else {
                    require __DIR__ . '/templates/error.php';
                }
            } else {
                require __DIR__ . '/templates/error.php';
            }
        }
        
        function crearprof(){
            $id = recoge('id');
            $nombre = recoge('nombre');
            if (getmodelo() != null) {
                $modelo = getModelo();
                
                if ($modelo->existe()) {
                    $profesor = new Profesor($id, $nombre);
                    //echo "<table><tr><td>".$profesor->getId()."</td><td>".$profesor->getNombre()."</td></tr></table>";
                    $modelo->createProfesor($profesor);
                    $params = array('profesores' => $modelo->readProfesor(), 'id' =>$modelo->getId("profesores"));
                    require __DIR__. '/templates/VistaProfesores.php';
                } else {
                    require __DIR__ . '/templates/error.php';
                }
            } else {
                require __DIR__ . '/templates/error.php';
            }
        }
        
        function upprof(){
            $id= recoge('id');
            $nombre= recoge('nombre');
            
            if (getmodelo() != null) {
                $modelo = getModelo();
                
                if ($modelo->existe()) {
                   $profesor = new Profesor($id,$nombre);
                   
                    $modelo->updateProfesor($profesor);

                    $params = array('profesores' => $modelo->readProfesor(), 'id' =>$modelo->getId("profesores"));
                    require __DIR__. '/templates/VistaProfesores.php';
                } else {
                    require __DIR__ . '/templates/error.php';
                }
            } else {
                require __DIR__ . '/templates/error.php';
            }
        }
        
        function editprof(){
            $id= recoge('id');
            $nombre= recoge('nombre');
            
            if (getmodelo() != null) {
                $modelo = getModelo();
              
                    if ($modelo->existe()) {
                        $profesor = new Profesor($id,$nombre);
                        $params = array('profesor' => $profesor);
                        require __DIR__. '/templates/editarProfesor.php';
                    } else {
                        require __DIR__ . '/templates/error.php';
                    }
            } else {
                require __DIR__ . '/templates/error.php';
            }
        }
        
                //      ACCIONES ASIGNATURA       //        
            
        function readasig(){
            if (getModelo() != null) {
                $modelo = getModelo();
                if ($modelo->existe()) {
                    $params = array('asignaturas' => $modelo->readAsignatura(), 'profesores' => $modelo->readProfesor(),'id' =>$modelo->getId("asignaturas"));
                    require __DIR__ . '/templates/VistaAsignaturas.php';
                } else {
                    require __DIR__ . '/templates/error.php';
                }
            }else {
                require __DIR__ . '/templates/error.php';
            }
        }
        
        function crearasig(){
            $id = recoge('id');
            $nombre = recoge('nom');
            $horas = recoge('hora');
            $Idprofesor = recoge ('prof');
            
            if (getmodelo() != null) {
                $modelo = getModelo();
                
                if ($modelo->existe()) {
                    $profesor1 = new Profesor ($Idprofesor, "");
                    $profesor2 = $modelo->getProfesor($profesor1);        
                    $asignatura = new Asignatura($id, $nombre, $horas, $profesor2);
                    
                    if($Idprofesor != "defecto"){
                        $modelo->createAsignatura($asignatura);

                    }
                    
                    $params = array('asignaturas' => $modelo->readAsignatura(), 'profesores' => $modelo->readProfesor(),'id' =>$modelo->getId("asignaturas"));
                    require __DIR__. '/templates/VistaAsignaturas.php';
                } else {
                    require __DIR__ . '/templates/error.php';
                }
            } else {
                require __DIR__ . '/templates/error.php';
            }
        }
        
        function delasig(){
            $id = recoge('id');
                if (getmodelo() != null) {
                    $modelo = getModelo();
                if ($modelo->existe()) {
                    $asignatura = new Asignatura($id, "", "","");
                    
                    $modelo->deleteAsignatura($asignatura);
                    
                    $params = array('asignaturas' => $modelo->readAsignatura(), 'profesores' => $modelo->readProfesor(),'id' =>$modelo->getId("asignaturas"));
                    $params['mensaje'] = array('tipo' => 'success', 'texto' => 'Asignatura ' . $asignatura->getNombre() . ' eliminada con éxito.');
                    require __DIR__ . '/templates/VistaAsignaturas.php';
                } else {
                    require __DIR__ . '/templates/error.php';
                }
            } else {
                require __DIR__ . '/templates/error.php';
            }
            
        }
        
                       
        function upasig(){
            $id = recoge('id');
            $nombre = recoge('nombre');
            $horas = recoge('horas');
            $Idprofesor = recoge ('idp');
            
            if (getmodelo() != null) {
                $modelo = getModelo();
                
                if ($modelo->existe()) {
                    $profesor1 = new Profesor ($Idprofesor, "");
                    $profesor2 = $modelo->getProfesor($profesor1);        
                    $asignatura = new Asignatura($id, $nombre, $horas, $profesor2->getId());
                   
                    $modelo->udpdateAsignatura($asignatura);     

                    $params = array('asignaturas' => $modelo->readAsignatura(), 'profesores' => $modelo->readProfesor(),'id' =>$modelo->getId("asignaturas"));
                    $params['mensaje'] = array('tipo' => 'success', 'texto' => 'Asignatura ' . $asignatura->getNombre() . ' eliminada con éxito.');
                    require __DIR__ . '/templates/VistaAsignaturas.php';
                } else {
                    require __DIR__ . '/templates/error.php';
                }
            } else {
                require __DIR__ . '/templates/error.php';
            }
            
        }
        
        function editasig(){
            $id = recoge('id');
            $nombre = recoge('nombre');
            $horas = recoge('horas');
            $Idprofesor = recoge ('idp');
            $Nprofesor = recoge ('nprof');
            if (getmodelo() != null) {
                $modelo = getModelo();
                
                if ($modelo->existe()) {
                    $profesor2 = new Profesor($Idprofesor,$Nprofesor);
                    $asignatura = $asignatura = new Asignatura($id, $nombre, $horas, $profesor2);

                    $params = array('asignatura' => $asignatura, 'profesores' => $modelo->readProfesor());
                    require __DIR__. '/templates/editarAsignatura.php';
                } else {
                    require __DIR__ . '/templates/error.php';
                }
            } else {
                require __DIR__ . '/templates/error.php';
            }
            
        }
        
        
        
        
        
        
    }          
            
?>


