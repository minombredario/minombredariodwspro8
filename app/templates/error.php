
<html>
    <head>
        <meta charset="UTF-8">
        <style type="text/css">
            .ups , .no-encontrada{
                text-align: center;
                font-size: 150px;
                color: #a89a00;
                text-shadow: -3px 5px 1px black;
                
                
            }
            
            .no-encontrada{
                font-size: 100px;
            }
            .centrado{
                display:inline;
            }
            img{
                width: 150px;
                display: block;
                margin: auto;
            }
            .mensaje{
                text-align: center;
                display: block;
                font-size: 25px;
            }
        </style>
        <title></title>
    </head>
    <body>
        <div class="centrado">
            <div class="alert"><img src="../web/images/images.jpg" /></div>
            <div class="ups">UPS!</div>
        </div>
        <div class="no-encontrada"> Página no encontrada</div>
        <div class="mensaje">
            <p>Parece que ha habido un error con la página que estabas buscando.</p>
            <p>Es posible que la entrada haya sido eliminada o que la dirección no exista.</p>
        </div>
    </body>
</html>
