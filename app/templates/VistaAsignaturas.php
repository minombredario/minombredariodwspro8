<?php 
                      
    ob_start(); 
    conectar();
    include("Config.php");
            
?>
            
    <table width="50%" border="0" align="center">
        <tr >
            <td class="primera_fila">Id</td>
            <td class="primera_fila">Nombre</td>
            <td class="primera_fila">Horas</td>
            <td class="primera_fila">Profesor</td>
            <td class="sin">&nbsp;</td>
            <td class="sin">&nbsp;</td>
            <td class="sin">&nbsp;</td>
        </tr> 
            <?php foreach($params['asignaturas'] as $asignatura): ?>
        <tr>
            <td ><?php echo $asignatura->getId();?></td>
            <td style="text-align: left" ><?php echo $asignatura->getNombre();?></td>
            <td><?php echo $asignatura->getHoras();?></td>
            <td><?php echo $asignatura->getProfesor()->getNombre();?></td>
                   
            <td class="bot"><a href="index.php?ctl=delasig&id=<?php echo $asignatura->getId()?>"><input type='button' name='delasig' id='del' value='Borrar'></a></td>
            <td class='bot'><a href="index.php?ctl=editasig&id=<?php echo $asignatura->getId()?>&nombre=<?php echo $asignatura->getNombre()?>&horas=<?php echo $asignatura->getHoras();?>&idp=<?php echo $asignatura->getProfesor()->getId();?>&nprof=<?php echo $asignatura->getProfesor()->getNombre();?>"><input type='button' name='up' id='up' value='Actualizar'></a></td>
                      
        </tr>

            <?php endforeach;?> 
            <form action="index.php?ctl=crearasig" method="POST">  
        <tr>
            <td><input type='hidden' name='id' size='10' class='centrado' value="<?php echo $params['id']?>" readonly="readonly" style="fon"></td>
            <td><input type='text' name='nom' size='60' class='centrado' required></td>
            <td><input type='text' name='hora' size='10' class='centrado' required></td>
            <td>
                <select name="prof" id="profesores">
                    <option value="defecto" selected="selected">Seleciona profesor</option>
                <?php foreach($params['profesores'] as $profesor): ?>
                    <option value="<?php echo $profesor->getId();?>"><?php echo $profesor->getNombre();?></option>
                <?php endforeach; ?>       
                </select>
            </td>
            <td class='bot'><input type='submit' name='crearasig' id='cr' value='Insertar'></td>
               
        </tr>
            </form>
    </table>
        
    <div style="position: relative; margin-left: 50%">
               
        <a href='index.php?ctl=menu'>Volver</a>
    </div>
        
    <p>&nbsp;</p>
        
<?php 

    $content = ob_get_clean();
    $titulo = 'Gestión de las Asignaturas';
    include 'layout.php';
?>