<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/hoja.css">
        
        <title>Ceedcv</title>
    </head>
    <body>
        <div class="content">
            
            <h1><?php if(isset($titulo)){echo $titulo;} ?></h1>
            
            <div>
                
                <?php echo $content ?> 
                
            </div>
       </div>
    </body>
  
        <footer>
            <div style="display: flex; flex-flow: row wrap; text-align: center" >
                <div style="width:18%;">
                    <?php echo Config::$empresa ?>
                </div>
                <div style="width:60%;">
                    <?php echo Config::$autor ."&nbsp&nbsp". Config::$fecha ?>
                </div>
                <div style="width:18%;">
                    <?php echo Config::$curso ?>
                </div>
            </div>

        </footer>
    
</html>
