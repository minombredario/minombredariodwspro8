<?php 
    require ("Config.php");
    ob_start(); 
    conectar();
    
    if(!isset($_SESSION)) { session_start();}
    $vista= $_SESSION["modelo"];
?>
<div class="logout">
    <a href="index.php?ctl=salir">Cerrar sesión, <span><?php echo $_SESSION['usuario'];?></span></a>
</div>

    <table width="50%" border="0" align="center">
              
        <thead>
            <tr>
                <th>
                    <h2><?php echo Config::$tema[$vista] ?></h2>
                </th>
            </tr>
        </thead>
                             
            <tr>
               <td class="primera_fila inicio">
                    Elegir: <?php if (isset($params['mensaje'])){ echo $params['mensaje']['texto'];}?>
                </td>
            </tr> 
            
            <tr>
                <td style="text-align: align;">
                    <a href="index.php?ctl=prof"><button class="boton">Gestión Profesor</button></a>
                    <a href="index.php?ctl=asig"><button class="boton">Gestión Asignatura</button></a>
                    <a href="index.php?ctl=inst"><button class="boton">Instalar BBDD</button></a>
                </td>
            </tr>
            
            <tr >
                <td class="primera_fila inicio">
                    Documentación por tema:
                </td>
            </tr> 
                
            <tr>
                <td class="resto_filas">
                <?php foreach(Config::$Enunciado as $doc){ 
                    echo "<li><a href='documentacion/".$doc.".pdf'  target='_blank'>". $doc ." Enunciado</a></li>";
                }?>
                </td>
            </tr>
                
            <tr>
                <td class="primera_fila inicio">
                    Documentación de este proyecto:
                </td>
            </tr>
                
            <tr>
                <td class="resto_filas">
                    <a href="documentacion/<?php echo Config::$Documentacion[$vista];?>_documentacion.pdf" target='_blank'>Documentación</a>
                </td>
            </tr>
        </table>
        <div style="position: relative; margin: auto; padding: 5px; width: 150px;">
            <a href="index.php?ctl=inicio"><button>Cambiar Base de datos</button></a>
        </div>
<?php 

    $content = ob_get_clean();
    include 'layout.php';
    
?>