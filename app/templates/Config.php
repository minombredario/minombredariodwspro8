<?php

    class Config {

        static public $titulo = "Gestión de Asignaturas";

        static public $autor = "Darío Navarro";

        static public $fecha = "19/01/17";

        static public $empresa = "CEEDCV";

        static public $curso = "2016-17";

        static public $tema = array("Ficheros" => "Tema 5. Ficheros", "Sql" => "Tema 8. MVC");

        static public $modelo = "mysql";

        static public $bdhostname = "localhost";

        static public $bdnombre = "ceedcv";

        static public $bdusuario = "alumno";

        static public $bdclave = "alumno";
        
        static public $Enunciado = array("Tema5.POO","Tema6.MYSQL","Tema7.SESIONES","Tema8.MVC");
        
        static public $Documentacion = array("Ficheros" => "Tema5", "Sql" => "Tema6");
    }
?>

