<?php

    // web/index.php
    //Inicio de la sesión
        session_start();
    
    // carga del modelo y los controladores
    //En las líneas 10-20 se realiza la carga de la configuración del modelo y de los controladores.   
    require_once __DIR__ . '/../app/modelo/Asignatura.php';
    require_once __DIR__ . '/../app/modelo/Profesor.php';
    require_once __DIR__ . '/../app/modelo/Usuario.php';
    require_once __DIR__ . '/../app/modelo/ModeloFichero.php';
    require_once __DIR__ . '/../app/modelo/ModeloSQL.php';
    require_once __DIR__ . '/../app/modelo/iModelo.php';

    require_once __DIR__ . '/../app/utils/funciones.php';
    
    require_once __DIR__ . '/../app/Controlador.php';
    require_once __DIR__ . '/../app/accion.php';
    
    
    // enrutamiento
    
     /* En las líneas 30-60 se declara un array asociativo cuya función es definir una tabla para
      * mapear (asociar), rutas en acciones de un controlador. Esta tabla será utilizada a continuación
      * para saber qué acción se debe disparar.
      */
    
    $map = array(
        //rutas generales
        'inicio' => array('controller' =>'Controlador', 'action' =>'inicio'),
        'login'  => array('controller' =>'Controlador', 'action' =>'login'),
        'salir' => array('controller' =>'Controlador', 'action' => 'logout'),
        'error' => array('controller' =>'Controlador', 'action' => 'error'),
        
        'registro' => array ('controller'=>'Controlador', 'action'=>'registro'),
        'registrar' => array ('controller'=>'accion', 'action'=>'registrar'),
        'validar' => array ('controller'=>'accion', 'action'=>'loguearse'),
        'menu'=> array('controller'=>'Controlador', 'action' => 'menu'),
        
        
        //rutas de base de datos
        'inst' => array('controller' =>'Controlador', 'action' => 'instalar'),
        'rell' => array('controller' =>'Controlador', 'action' => 'ejemplo'),

        //rutas de asignatura
        'asig' => array('controller' =>'accion', 'action' =>'readasig'),
        'crearasig' => array('controller' =>'accion', 'action' =>'crearasig'),
        'upasig' => array('controller' =>'accion', 'action' =>'upasig'),
        'editasig' => array('controller' =>'accion', 'action' =>'editasig'),
        'delasig' => array('controller' =>'accion', 'action' =>'delasig'),

        //rutas de profesores
        'prof' => array('controller' =>'accion', 'action' =>'readprof'),
        'crearprof' => array('controller' =>'accion', 'action' =>'crearprof'),
        'upprof' => array('controller' =>'accion', 'action' =>'upprof'),
        'editprof' => array('controller' =>'accion', 'action' =>'editprof'),
        'delprof' => array('controller' =>'accion', 'action' =>'delprof')
    );
    
    // Parseo de la ruta
    /*
     * En las líneas 72-90 se lleva a cabo el parseo de la URL y la carga de la acción, si la ruta está
     * definida en la tabla de rutas. En caso contrario se devuelve una página de error. Observa que
     * hemos utilizado la función header() de PHP para indicar en la cabecera HTTP el código de
     * error correcto. Además enviamos un pequeño documento HTML que informa del error.
     * también definimos a inicio como una ruta por defecto, ya que si la query string llega vacía,
     * se opta por cargar esta acción.
     */
    
      
    if (isset($_GET['ctl'])) {

        if (isset($map[$_GET['ctl']])) {

            $ruta = $_GET['ctl'];

        } else {
            //si la opcio elegida no existe en el array $map, asignamos el valor error para que redirija a la pagina de error.
            $ruta = 'error';
               
        }

    } else {
            //si no hay seleccion, redirige a la pantalla principal.
            $ruta = 'inicio';

    }
    
    $controlador = $map[$ruta];
    
    
    // Ejecución del controlador asociado a la ruta

    if (method_exists($controlador['controller'],$controlador['action'])) {

        call_user_func(array(new $controlador['controller'], $controlador['action']));

    } else {

       call_user_func(array('Controlador', 'error'));

    }
    